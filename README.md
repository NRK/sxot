# sxot - Simple X screenshOT

`sxot` is an extremely simple screenshotting tool for X11 designed with the unix
philosophy in mind. This means that *by itself* `sxot` doesn't do much - it
simply takes a screenshot and outputs a binary [ppm][] file to stdout. `sxot` is
meant to be used in conjunction with other tools in order to be useful in
practice.

[ppm]: https://en.wikipedia.org/wiki/Netpbm#PPM_example

## Usage

`sxot` supports only three options:

* `-c, --cursor` adds the cursor to the screenshot.
* `-g, --geom <x,y,w,h>` which tells `sxot` to capture the specified rectangle
  instead of screenshotting the entire screen.
* `-f, --force` overwrites the output file if it already exists.

Some example usage:

* Taking a full screen screenshot and saving it to `out.ppm`:

```console
$ sxot > out.ppm
```

* Screenshoting a selected area with [`selx`][selx]:

```console
$ sxot -g $(selx) > out.ppm
```

* Screenshoting with a 5 seconds delay with [`sleep`][sleep]
  and saving it with a file-name that has date and time information
  with [`date`][date]:

```console
$ sleep 5 && sxot > $(date '+%F_%T').ppm
```

* Screenshoting with a 2 seconds delay after selection with [`slop`][slop]:

```console
$ sxot --geom $(slop -f "%x,%y,%w,%h"; sleep 2;) > out.ppm
```

* Saving a png screenshot using [`ffmpeg`][ffmpeg]:

```console
$ sxot | ffmpeg -i - out.png
```

Or using [`ImageMagick`][imagemagick]:

```console
$ sxot | convert - out.png
```

* Copying an optimized png screenshot to clipboard,
  using [`optipng-pipe`][op-pipe] and [`xclip`][xclip]:

```console
$ sxot | optipng-pipe | xclip -selection clipboard -target image/png
```

* Screenshoting a specific monitor via [`selx`][selx]:

```console
$ sxot -g $(selx -m 0) > monitor0.ppm
```

[selx]: https://codeberg.org/NRK/selx
[slop]: https://github.com/naelstrof/slop
[xclip]: https://github.com/astrand/xclip
[sleep]: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/sleep.html
[date]: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/date.html
[ffmpeg]: https://ffmpeg.org
[imagemagick]: https://imagemagick.org/index.php

## Output formats

`sxot` only supports ppm format. Instead of trying to support complicated
compressed formats and doing a mediocre job, `sxot` leaves this task to more
specialized tools such as [`optipng`][optipng].

Some recommended formats and tools:

* I suggest using png with [`optipng-pipe`][op-pipe] for best compatibility and
  reasonably low file-sizes:

```console
$ sxot | optipng-pipe > out.png
```

* If smaller file-sizes are desired, then I can suggest using lossless webp with
  [`cwebp`][cwebp]. Keep in mind that some compatibility is lost since older
  software might not support webp:

```console
$ sxot | cwebp -quiet -lossless -z 8 -mt -o out.webp -- -
```

The `cwebp` tool comes with `libwebp`, however your distro might package it
separately. Conveniently `cwebp` accepts stdin if the file is named "-",
although this feature is undocumented.

- - -

`jxl` also [seems promising][lossless-bench] in terms of lossless compression.
But compatibility isn't that great as of now (party due to
[google killing it][jxl_is_kil] in chromium).

[optipng]: https://optipng.sourceforge.net
[op-pipe]: ./etc/optipng-pipe
[cwebp]: https://developers.google.com/speed/webp/docs/cwebp
[lossless-bench]: https://siipo.la/blog/whats-the-best-lossless-image-format-comparing-png-webp-avif-and-jpeg-xl
[jxl_is_kil]: https://www.fsf.org/blogs/community/googles-decision-to-deprecate-jpeg-xl-emphasizes-the-need-for-browser-choice-and-free-formats

## Dependencies

- Build Dependencies:
  * C11 compiler
  * POSIX 2001 compatible C standard library.
  * Necessary library headers (on some distros you need to install `*-dev`
    packages to get header files)

- Runtime Dependencies:
  * X11 server (with true color enabled)
  * Xlib
  * Xfixes

## Building

* Simple release build:

```console
$ cc -o sxot sxot.c -O3 -s -l X11 -l Xfixes
```

The above command should also work with `gcc`, `clang` or any other C compiler
that has a POSIX compatible cli interface.

* Debug build with `gcc` (also works with `clang`):

```console
$ gcc -o sxot sxot.c -std=c11 -Wall -Wextra -Wpedantic -Wshadow \
    -g3 -D DEBUG -O0 -fsanitize=address,undefined -l X11 -l Xfixes
```

* If you're editing the code, you may optionally run some static analysis:

```console
$ make -f etc/analyze.mk
```

## Installing

Simply copy the binary over to your `$PATH`:

```console
# install -Dm755 sxot /usr/local/bin/sxot
```

A zsh completion script is also available for zsh users under
[etc/zsh-completion](./etc/zsh-completion/).
