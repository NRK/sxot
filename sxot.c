/*
 * Copyright (C) 2023-2024 NRK.
 *
 * This file is part of sxot.
 *
 * sxot is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * sxot is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sxot. If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200112L  // NOLINT

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <limits.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xfixes.h>

typedef uint8_t     u8;
typedef uint32_t    u32;
typedef uint64_t    u64;
typedef ptrdiff_t   Size;
typedef struct { u8 *s; Size len; } Str;
typedef struct { u8 *buf, *off, *end; int fd, err; } Out;

#define NOP()           ((void)0)
#define SIZEOF(...)     ((Size)sizeof(__VA_ARGS__))
#define ARRLEN(...)     (SIZEOF(__VA_ARGS__) / SIZEOF(0[__VA_ARGS__]))
#define S(X)            ((Str){ .s = (u8 *)(X), .len = ARRLEN(X) - 1 })
#define SC(X)           { .s = (u8 *)(X), .len = ARRLEN(X) - 1 }

#ifdef __GNUC__
	// when debugging, use gcc/clang and compile with UBSan enabled:
	// `-fsanitize=undefined -fsanitize-trap`
	#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())
#else
	#define ASSERT(X)  ((void)0)
#endif

static Str
str_from_cstr(char *s)
{
	Str ret = { .s = (u8 *)s, 0 };
	if (s) for (NOP(); s[ret.len] != '\0'; ++ret.len) {}
	return ret;
}

static bool
str_eq(Str a, Str b)
{
	Size n = -1;
	ASSERT(a.len >= 0 && b.len >= 0);
	if (a.len == b.len) {
		for (n = 0; n < a.len && a.s[n] == b.s[n]; ++n) {}
	}
	return n == a.len;
}

static Str
str_tok(Str *s, u8 ch)
{
	Str ret = { .s = s->s };
	ASSERT(!(s->len < 0));
	for (NOP(); ret.len < s->len && ret.s[ret.len] != ch; ++ret.len) {}
	s->s   += ret.len + (ret.len < s->len);
	s->len -= ret.len + (ret.len < s->len);
	return ret;
}

static bool
str_to_int(Str s, int *out)
{
	u64 n = 0;
	ASSERT(!(s.len < 0));
	for (Size i = 0; i < s.len; ++i) {
		u8 ch = s.s[i];
		n = (n * 10) + (ch - '0');
		if (n > INT_MAX || !(ch >= '0' && ch <= '9')) {
			return false;
		}
	}
	*out = n;
	return s.len > 0;
}

static int
full_write(int fd, Str s)
{
	while (s.len > 0) {
		Size res = write(fd, s.s, s.len);
		if (res < 0) {
			return -1;
		}
		s.s += res; s.len -= res;
	}
	return 0;
}

static Out
out_create(int fd, u8 *buf, u8 *end)
{
	ASSERT(buf != NULL && end != NULL && buf < end);
	return (Out){ .fd = fd, .buf = buf, .off = buf, .end = end };
}

static void
out_flush(Out *out)
{
	Size n = out->off - out->buf;
	out->off = out->buf;
	out->err = out->err || full_write(out->fd, (Str){out->buf, n}) < 0;
}

static void
out_str(Out *out, Str str)
{
	u8 *s = str.s;
	u8 *end = (str.len > 0) ? s + str.len : s;
	if (!out->err && str.len >= (out->end - out->buf)) { // fast path for large writes
		out_flush(out);
		if (!out->err) {
			out->err = full_write(out->fd, str) < 0;
		}
	} else while (!out->err && s < end) {
		if (out->off == out->end) {
			out_flush(out);
		} else while (out->off < out->end && s < end) {
			*out->off++ = *s++;
		}
	}
}

static void
out_uint(Out *out, u64 n)
{
	u8 buf[32], *p = buf + ARRLEN(buf), *end = p;
	do { *--p = '0' + (n % 10); } while (n /= 10);
	out_str(out, (Str){ p, end - p });
}

_Noreturn static void
fatal(Out *eout, Str *sv, Size n)
{
	for (Size i = 0; i < n; ++i) out_str(eout, sv[i]);
	out_str(eout, S("\n"));
	out_flush(eout);
	exit(1);
}
#define fatal(O, ...) fatal((O), (Str []){ __VA_ARGS__ }, ARRLEN((Str []){ __VA_ARGS__ }))

static void
write_ppm(u8 *data, int width, int height, bool msb_first, Out *out)
{
	out_str(out, S("P6\n"));
	out_uint(out, width);  out_str(out, S(" "));
	out_uint(out, height); out_str(out, S("\n"));
	out_str(out, S("255\n"));

	u8 *w = data;
	u8 *end = data + ((Size)width * height * 4);
	for (u8 *p = data; p < end; p += 4) {
		int R, G, B;
		if (msb_first) {
			R = p[1]; G = p[2]; B = p[3];
		} else {
			R = p[2]; G = p[1]; B = p[0];
		}
		*w++ = R; *w++ = G; *w++ = B;
	}
	ASSERT(w < end);
	out_str(out, (Str){ data, w - data });
}

static void
blend_cursor(XImage *im, XFixesCursorImage *cur)
{
	int yend = (cur->y + cur->height) < im->height ?
	           (cur->y + cur->height) : im->height;
	int xend = (cur->x + cur->width) < im->width ?
	           (cur->x + cur->width) : im->width;
	int R, G, B;
	if (im->byte_order == MSBFirst) {
		R = 1; G = 2; B = 3;
	} else {
		R = 2; G = 1; B = 0;
	}
	for (int y = cur->y, yc = 0; y < yend; ++y, ++yc) {
		for (int x = cur->x, xc = 0; x < xend; ++x, ++xc) {
			Size off = ((Size)y * im->width) + x;
			u8 *p = (u8 *)im->data + (off * 4);
			u32 c = cur->pixels[(yc * cur->width) + xc];
			u32 a = (c >> 24) & 0xFF;
			if (a == 0) {
				continue;
			}
			p[R] = ((((c >> 16) & 0xFF) * a) + (p[R] * (0xFF - a))) / 0xFF;
			p[G] = ((((c >>  8) & 0xFF) * a) + (p[G] * (0xFF - a))) / 0xFF;
			p[B] = ((((c >>  0) & 0xFF) * a) + (p[B] * (0xFF - a))) / 0xFF;
		}
	}
}

static bool
parse_geom(int v[static 4], Str s)
{
	for (int *end = v + 4; v < end; ++v) {
		if (!str_to_int(str_tok(&s, ','), v) || *v < 0) {
			return false;
		}
	}
	return s.len == 0;
}

extern int
main(int argc, char *argv[])
{
	static const Str usage = SC(
		"Usage: sxot [OPTIONS..] [FILE]\n"
		"Capture a binary ppm screenshot and writes it to FILE.\n"
		"Writes to stdout by default if no FILE is given.\n"
		"\n"
		"OPTIONS:\n"
		"  -g, --geom <x,y,w,h>   Capture the specified rectangle\n"
		"  -c, --cursor           Capture the cursor also\n"
		"  -f, --force            Overwrite FILE if it already exists."
	);
	static const Str version = SC(
		"sxot v0.2.0\n"
		"Copyright (C) 2023-2024 NRK.\n"
		"License: GPLv3+ <https://gnu.org/licenses/gpl.html>\n"
		"Upstream: <https://codeberg.org/NRK/sxot>"
	);

	static u8 out_buffer[1<<16];
	static u8 eout_buffer[1<<8];
	struct {
		union {
			int v[4];
			struct { int x, y, w, h; };
		};
		Out  out[1];
		Out  eout[1];
		const char *output_filename;
		bool capture_cursor;
		bool overwrite_file;
	} opt = { .x = -1 };
	struct { Display *dpy; Window root; } x11;

	opt.eout[0] = out_create(2, eout_buffer, 1[&eout_buffer]);

	if ((x11.dpy = XOpenDisplay(NULL)) == NULL) {
		fatal(opt.eout, S("failed to open X Display"));
	}
	x11.root = DefaultRootWindow(x11.dpy);

	for (int i = 1; i < argc; ++i) {
		Str arg = str_from_cstr(argv[i]);
		if (str_eq(arg, S("--geom")) || str_eq(arg, S("-g"))) {
			if (!parse_geom(opt.v, str_from_cstr(argv[++i]))) {
				fatal(opt.eout, S("invalid geometry"));
			}
		} else if (str_eq(arg, S("--cursor")) || str_eq(arg, S("-c"))) {
			opt.capture_cursor = true;
		} else if (str_eq(arg, S("--force")) || str_eq(arg, S("-f"))) {
			opt.overwrite_file = true;
		} else if (str_eq(arg, S("--help")) || str_eq(arg, S("-h"))) {
			fatal(opt.eout, usage);
		} else if (str_eq(arg, S("--version")) || str_eq(arg, S("-v"))) {
			fatal(opt.eout, version);
		} else if (arg.len > 0 && arg.s[0] == '-') {
			fatal(opt.eout, S("unknown flag: `"), arg, S("`"));
		} else if (opt.output_filename == NULL) {
			opt.output_filename = (const char *)arg.s;
		} else {
			fatal(opt.eout, S("excess argument: `"), arg, S("`"));
		}
	}

	if (opt.output_filename == NULL) {
		opt.out[0] = out_create(1, out_buffer, 1[&out_buffer]);
	} else {
		int flags = O_WRONLY|O_CREAT|O_EXCL;
		if (opt.overwrite_file) {
			flags &= ~O_EXCL;
			flags |= O_TRUNC;
		}
		int fd = open(opt.output_filename, flags, 0600);
		if (fd < 0) {
			fatal(
				opt.eout, S("couldn't open output file: "),
				str_from_cstr(strerror(errno))
			);
		}
		opt.out[0] = out_create(fd, out_buffer, 1[&out_buffer]);
	}

	if (isatty(opt.out->fd)) {
		fatal(opt.eout, S("aborting: output is connected to a terminal"));
	}

	if (opt.x < 0) {
		XWindowAttributes tmp;
		if (XGetWindowAttributes(x11.dpy, x11.root, &tmp) == 0) {
			fatal(opt.eout, S("XGetWindowAttributes failed"));
		}
		opt.x = tmp.x;
		opt.y = tmp.y;
		opt.h = tmp.height;
		opt.w = tmp.width;
	}

	XImage *im = XGetImage(
		x11.dpy, x11.root, opt.x, opt.y, opt.w, opt.h,
		AllPlanes, ZPixmap
	);
	if (im == NULL) {
		fatal(opt.eout, S("XGetImage failed"));
	}
	if (im->bits_per_pixel != 32 || im->bytes_per_line != (im->width * 4) ||
	    !(im->depth == 24 || im->depth == 32))
	{
		fatal(opt.eout, S("unsupported XImage format"));
	}

	if (opt.capture_cursor) {
		if (XFixesQueryVersion(x11.dpy, (int []){0}, (int []){0}) != True) {
			fatal(opt.eout, S("XFixesQueryVersion failed"));
		}
		XFixesCursorImage *cur = XFixesGetCursorImage(x11.dpy);
		if (cur == NULL) {
			fatal(opt.eout, S("XFixesGetCursorImage failed"));
		}
		blend_cursor(im, cur);
#ifdef DEBUG
		XFree(cur);
#endif
	}

	write_ppm((u8 *)im->data, im->width, im->height, im->byte_order == MSBFirst, opt.out);

	out_flush(opt.out);
	if (opt.out->err) {
		fatal(opt.eout, S("error writing to output"));
	}

#ifdef DEBUG
	XDestroyImage(im);
#endif
	XCloseDisplay(x11.dpy);

	return 0;
}

// TODO?: add farbfeld support
// TODO?: support for more XImage formats
