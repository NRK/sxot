# convenient makefile to run some static analyzers:
#	$ make -f etc/analyze.mk

nproc != nproc
MAKEFLAGS := -j$(nproc)

analyze: analyze-gcc analyze-cppcheck analyze-clang-tidy analyze-clang-weverything
analyze-gcc:
	gcc sxot.c -o /dev/null -c -std=c11 -Wall -Wextra -Wpedantic -fanalyzer \
		-Ofast -fwhole-program
analyze-cppcheck:
	cppcheck sxot.c --std=c11 --quiet --inline-suppr --force \
		--enable=performance,portability,style --max-ctu-depth=16 \
		--suppress=constVariable
analyze-clang-tidy:
	clang-tidy --config-file=./etc/.clang-tidy sxot.c --quiet -- -std=c11
analyze-clang-weverything:
	clang sxot.c -o /dev/null -c -std=c11 -Ofast -Weverything \
		-Wno-unreachable-code-break -Wno-comma -Wno-padded \
		-Wno-disabled-macro-expansion -Wno-declaration-after-statement \
		-Wno-shorten-64-to-32 -Wno-implicit-int-conversion \
		-Wno-sign-conversion -Wno-unsafe-buffer-usage

.PHONY: analyze
